﻿//////////////////////////////////////////////////////////////////////////////////
//
// Project            : AutoServiceDiscovery
// Description        : Test for resolver.
//
// Copyright          : (c) 2015 Torsten Bär
//
// Published under the MIT License. See license.rtf or http://www.opensource.org/licenses/mit-license.php.
//
//////////////////////////////////////////////////////////////////////////////////

namespace ToBaer.CSharp.AutoServiceDiscovery.Tests
{
   using System;

   using AutoServiceDiscovery.MetadataResolver;

   using Contracts;

   using log4net;

   using NUnit.Framework;

   [TestFixture]
   public class MetadataResolver
   {
      private static readonly ILog Log = LogManager.GetLogger(typeof(MetadataResolver));

      [Test]
      public void ShouldResolveMetadataFromService()
      {
         using (var host = new ServiceHost<ITestService, TestServiceMock>())
         {
            host.Run();

            Log.Debug("Resolving host");

            var endpoint = ServiceMetadataResolver.ResolveEndpointForNetTcp<ITestService>(host.ServiceAddress.AbsoluteUri);

            Assert.That(endpoint, Is.Not.Null);

            Log.DebugFormat("Endpoint resolved; {0}", endpoint.Address.Uri);

            Assert.That(endpoint.Address.Uri.AbsoluteUri, Is.SameAs(host.ServiceAddress.AbsoluteUri));
         }
      }
   }
}