//////////////////////////////////////////////////////////////////////////////////
//
// Project            : AutoServiceDiscovery
// Description        : Generic ServiceHost.
//
// Copyright          : (c) 2015 Torsten B�r
//
// Published under the MIT License. See license.rtf or http://www.opensource.org/licenses/mit-license.php.
//
//////////////////////////////////////////////////////////////////////////////////
namespace ToBaer.CSharp.AutoServiceDiscovery.Tests
{
   using System;
   using System.Net;
   using System.ServiceModel;
   using System.ServiceModel.Description;
   using System.ServiceModel.Discovery;

   using log4net;

   internal sealed class ServiceHost<TInterface, TService> : IDisposable
      where TService : TInterface
   {
      private static readonly ILog Log = LogManager.GetLogger(typeof(ServiceHost<TInterface, TService>));

      private readonly ServiceHost host;
      private Uri serviceAddress;

      public ServiceHost()
      {
         var address = Dns.GetHostName();

         ServiceAddress = new Uri(string.Format("net.tcp://{0}:42730/{1}", address, typeof(TInterface).Name));

         host = new ServiceHost(typeof(TService), ServiceAddress);

         var serviceMetadataBehavior = host.Description.Behaviors.Find<ServiceMetadataBehavior>() ?? new ServiceMetadataBehavior();
         serviceMetadataBehavior.HttpGetEnabled = false;
         serviceMetadataBehavior.MetadataExporter.PolicyVersion = PolicyVersion.Policy15;

         host.Description.Behaviors.Add(serviceMetadataBehavior);
         host.Description.Behaviors.Add(new ServiceDiscoveryBehavior());

         host.AddDefaultEndpoints();

         host.AddServiceEndpoint(new UdpDiscoveryEndpoint());
         host.AddServiceEndpoint(ServiceMetadataBehavior.MexContractName, MetadataExchangeBindings.CreateMexTcpBinding(), "mex");

         Log.DebugFormat("Host initialized; Enpoint {0}", ServiceAddress);
      }

      public Uri ServiceAddress
      {
         get { return serviceAddress; }
         private set { serviceAddress = value; }
      }

      public void Dispose()
      {
         host.Close();
      }

      public void Run()
      {
         Log.Debug("Opening host");

         host.Open();

         Log.Debug("Host opened");
      }
   }
}