﻿//////////////////////////////////////////////////////////////////////////////////
//
// Project            : AutoServiceDiscovery
// Description        : Implementation of service metadate resolver.
//
// Copyright          : (c) 2015 Torsten Bär
//
// Published under the MIT License. See license.rtf or http://www.opensource.org/licenses/mit-license.php.
//
//////////////////////////////////////////////////////////////////////////////////
namespace ToBaer.CSharp.AutoServiceDiscovery.MetadataResolver
{
   using System;
   using System.Collections.Generic;
   using System.Linq;
   using System.ServiceModel;
   using System.ServiceModel.Description;

   public static class ServiceMetadataResolver
   {
      static readonly Dictionary<string, IEnumerable<ServiceEndpoint>> endpointCache = new Dictionary<string, IEnumerable<ServiceEndpoint>>();

      public static IEnumerable<ServiceEndpoint> ResolveEndpoints<T>(string serviceUri)
      {
         IEnumerable<ServiceEndpoint> endpoints;
         if (ResolveFromCache<T>(serviceUri, out endpoints))
            return endpoints;

         return ResolveEndpoints<T>(serviceUri.ForUri().BuildMexEndpoint());
      }

      private static bool ResolveFromCache<T>(string serviceUri, out IEnumerable<ServiceEndpoint> enumerable)
      {
         return endpointCache.TryGetValue(serviceUri, out enumerable);
      }

      internal static IEnumerable<ServiceEndpoint> ResolveEndpoints<T>(EndpointAddress endpoint)
      {
         var binding = new BasicHttpBinding(BasicHttpSecurityMode.Transport);
         binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Windows;

         var serviceEndpoint = new ServiceEndpoint(ContractDescription.GetContract(typeof(T)), binding, endpoint);

         return endpointCache[serviceEndpoint.Address.Uri.AbsoluteUri] = MetadataResolver.Resolve(typeof(T), serviceEndpoint.Address);
      }

      public static ServiceEndpoint ResolveEndpointForNetTcp<T>(string serviceUri)
      {
         return ResolveBindingFor<T>(serviceUri, scheme => scheme == Uri.UriSchemeNetTcp);
      }

      public static ServiceEndpoint ResolveBindingFor<T>(string serviceUri, Predicate<string> uriScheme)
      {
         return ResolveEndpoints<T>(serviceUri).FirstOrDefault(serviceEndpoint => uriScheme(serviceEndpoint.Address.Uri.Scheme));
      }
   }
}