# Dynamic WCF service discovery #

This repository shows how to host and dynamic discover WCF services in your local network.

## How do I get set up? ##

This repository includes all you need to start. Simply build the binaries and you're ready.
No configuration, no dependencies.

**To start discovery sample**

1. Start 'AutoServiceDiscovery.ServiceHost.exe'
2. Start 'AutoServiceDiscovery.Client.exe'


## Who do I talk to? ##

* Repo owner