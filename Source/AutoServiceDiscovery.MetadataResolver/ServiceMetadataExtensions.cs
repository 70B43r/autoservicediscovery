//////////////////////////////////////////////////////////////////////////////////
//
// Project            : AutoServiceDiscovery
// Description        : Some extensions for uri manipulations.
//
// Copyright          : (c) 2015 Torsten B�r
//
// Published under the MIT License. See license.rtf or http://www.opensource.org/licenses/mit-license.php.
//
//////////////////////////////////////////////////////////////////////////////////
namespace ToBaer.CSharp.AutoServiceDiscovery.MetadataResolver
{
   using System;
   using System.ServiceModel;

   internal static class ServiceMetadataExtensions
   {
      public static UriBuilder ForUri(this string uri)
      {
         return new UriBuilder(uri);
      }

      public static UriBuilder UseScheme(this UriBuilder uriBuilder, string scheme)
      {
         uriBuilder.Scheme = scheme;

         return uriBuilder;
      }

      public static UriBuilder UseDefaultPort(this UriBuilder uriBuilder)
      {
         uriBuilder.Port = -1;

         return uriBuilder;
      }

      public static EndpointAddress BuildMexEndpoint(this UriBuilder uriBuilder)
      {
         uriBuilder.Path += "/mex";

         return new EndpointAddress(uriBuilder.Uri);
      }
   }
}