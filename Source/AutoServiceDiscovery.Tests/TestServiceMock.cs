﻿//////////////////////////////////////////////////////////////////////////////////
//
// Project            : AutoServiceDiscovery
// Description        : Service mock.
//
// Copyright          : (c) 2015 Torsten Bär
//
// Published under the MIT License. See license.rtf or http://www.opensource.org/licenses/mit-license.php.
//
//////////////////////////////////////////////////////////////////////////////////
namespace ToBaer.CSharp.AutoServiceDiscovery.Tests
{
   using Contracts;

   public class TestServiceMock : ITestService
   {
      public string HandleMessage(string message)
      {
         return message;
      }
   }
}