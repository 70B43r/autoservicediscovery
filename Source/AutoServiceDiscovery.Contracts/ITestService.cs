﻿//////////////////////////////////////////////////////////////////////////////////
//
// Project            : AutoServiceDiscovery
// Description        : Service contract.
//
// Copyright          : (c) 2015 Torsten Bär
//
// Published under the MIT License. See license.rtf or http://www.opensource.org/licenses/mit-license.php.
//
//////////////////////////////////////////////////////////////////////////////////

namespace ToBaer.CSharp.AutoServiceDiscovery.Contracts
{
   using System;
   using System.ServiceModel;

   [ServiceContract(Namespace = "urn:tobaer/autodiscover/service")]
   public interface ITestService
   {
      [OperationContract]
      string HandleMessage(string message);
   }
}
