﻿//////////////////////////////////////////////////////////////////////////////////
//
// Project            : AutoServiceDiscovery
// Description        : Service implementation.
//
// Copyright          : (c) 2015 Torsten Bär
//
// Published under the MIT License. See license.rtf or http://www.opensource.org/licenses/mit-license.php.
//
//////////////////////////////////////////////////////////////////////////////////
namespace ToBaer.CSharp.AutoServiceDiscovery.Service
{
   using System;

   using Contracts;

   public class TestService : ITestService
   {
      public string HandleMessage(string message)
      {
         Console.WriteLine(message);

         return String.Format("{0:G}: you've send me '{1}'. I'm sitting on machine {2}", DateTime.Now, message, Environment.MachineName);
      }
   }
}