﻿//////////////////////////////////////////////////////////////////////////////////
//
// Project            : AutoServiceDiscovery
// Description        : Discovery client.
//
// Copyright          : (c) 2015 Torsten Bär
//
// Published under the MIT License. See license.rtf or http://www.opensource.org/licenses/mit-license.php.
//
//////////////////////////////////////////////////////////////////////////////////

namespace ToBaer.CSharp.AutoServiceDiscovery.Client
{
   using System;
   using System.Linq;
   using System.ServiceModel;
   using System.ServiceModel.Description;
   using System.ServiceModel.Discovery;
   using System.Threading.Tasks;

   using Contracts;

   internal class Program
   {
      private static void Main(string[] args)
      {
         Console.WriteLine("Searching service");

         //
         // search service via udp discovery endpoint
         //
         FindResponse response;
         using (var searchTask = Task.Run(() =>
                                          {
                                             using (var discoveryClient = new DiscoveryClient(new UdpDiscoveryEndpoint()))
                                             {
                                                var criteria = FindCriteria.CreateMetadataExchangeEndpointCriteria(typeof(ITestService));
                                                // we need just on service
                                                criteria.MaxResults = 1;

                                                return discoveryClient.Find(criteria);
                                             }
                                          }))
         {
            while (!searchTask.Wait(TimeSpan.FromSeconds(1)))
               Console.Write(".");
            Console.WriteLine();

            if (searchTask.IsFaulted)
            {
               Console.WriteLine(searchTask.Exception);
               Console.WriteLine("press <ENTER> to leave");
               Console.ReadLine();
               Environment.Exit(-11);
            }

            response = searchTask.Result;
         }

         var endpoints = response.Endpoints;
         if (endpoints == null || endpoints.Count == 0)
         {
            Console.WriteLine("No endpoint found - press <ENTER> to leave");
            Console.ReadLine();
            Environment.Exit(-1);
         }

         var endpoint = endpoints.First();
         Console.WriteLine("Service found at: {0}", endpoint.Address.Uri.Host);
         Console.WriteLine("Resolving metadata");

         //
         // resolve metadata to get correct bindings
         //

         ServiceEndpointCollection metadataEntpoints;
         using (var metadataTask = Task.Run(() => MetadataResolver.Resolve(typeof(ITestService), endpoint.Address)))
         {
            while (!metadataTask.Wait(TimeSpan.FromSeconds(1)))
               Console.Write(".");
            Console.WriteLine();

            if (metadataTask.IsFaulted)
            {
               Console.WriteLine(metadataTask.Exception);
               Console.WriteLine("press <ENTER> to leave");
               Console.ReadLine();
               Environment.Exit(-12);
            }

            metadataEntpoints = metadataTask.Result;
         }

         if (metadataEntpoints.Count == 0)
         {
            Console.WriteLine("Could not resolve metadata");
            Console.WriteLine("press <ENTER> to leave");
            Console.ReadLine();
            Environment.Exit(-2);
         }

         var metadataEntpoint = metadataEntpoints.First();

         //
         // create channel and call service
         //

         var testService = ChannelFactory<ITestService>.CreateChannel(metadataEntpoint.Binding, metadataEntpoint.Address);

         using (testService as IDisposable)
         {
            var serviceResponse = testService.HandleMessage("Hello from " + Environment.MachineName);

            (testService as ICommunicationObject).Close();

            Console.WriteLine("Service responds: {0}", serviceResponse);
         }
         Console.WriteLine("press <ENTER> to leave");
         Console.ReadLine();
      }
   }
}