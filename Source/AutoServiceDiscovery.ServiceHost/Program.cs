﻿//////////////////////////////////////////////////////////////////////////////////
//
// Project            : AutoServiceDiscovery
// Description        : Servicehost.
//
// Copyright          : (c) 2015 Torsten Bär
//
// Published under the MIT License. See license.rtf or http://www.opensource.org/licenses/mit-license.php.
//
//////////////////////////////////////////////////////////////////////////////////

namespace ToBaer.CSharp.AutoServiceDiscovery.ServiceHost
{
   using System;
   using System.Net;
   using System.ServiceModel;
   using System.ServiceModel.Description;
   using System.ServiceModel.Discovery;

   using Contracts;

   using Service;

   public static class Program
   {
      public static void Main(string[] args)
      {
         var address = Dns.GetHostName();

         var netTcpAddress = new Uri(string.Format("net.tcp://{0}:47110/{1}", address, typeof(ITestService).Name));

         using (var host = new ServiceHost(typeof(TestService), netTcpAddress))
         {
            var serviceMetadataBehavior = host.Description.Behaviors.Find<ServiceMetadataBehavior>() ?? new ServiceMetadataBehavior();

            serviceMetadataBehavior.HttpGetEnabled = false;
            serviceMetadataBehavior.MetadataExporter.PolicyVersion = PolicyVersion.Policy15;

            host.Description.Behaviors.Add(serviceMetadataBehavior);
            host.Description.Behaviors.Add(new ServiceDiscoveryBehavior());

            host.AddDefaultEndpoints();

            host.AddServiceEndpoint(new UdpDiscoveryEndpoint());
            host.AddServiceEndpoint(ServiceMetadataBehavior.MexContractName, MetadataExchangeBindings.CreateMexTcpBinding(), "mex");

            host.Open();

            Console.WriteLine("service listening at {0}", netTcpAddress);
            Console.WriteLine("+++ service ready +++");
            Console.WriteLine("press <ENTER> to close");
            Console.ReadLine();
         }
      }
   }
}