﻿//////////////////////////////////////////////////////////////////////////////////
//
// Project            : AutoServiceDiscovery
// Description        : Setup for testing environment.
//
// Copyright          : (c) 2015 Torsten Bär
//
// Published under the MIT License. See license.rtf or http://www.opensource.org/licenses/mit-license.php.
//
//////////////////////////////////////////////////////////////////////////////////
namespace ToBaer.CSharp.AutoServiceDiscovery.Tests
{
   using log4net;
   using log4net.Appender;
   using log4net.Core;
   using log4net.Layout;

   using NUnit.Framework;

   [SetUpFixture]
   public class TestSetup
   {
      private static readonly ILog Log = LogManager.GetLogger(typeof(TestSetup));

      public TestSetup()
      {
         if (!LogManager.GetRepository().Configured)
         {
            log4net.Config.BasicConfigurator.Configure(new TraceAppender
                                                       {
                                                          Category = new PatternLayout("%logger{1}"),
                                                          Layout = new PatternLayout("%message%newline")
                                                       });

            LogManager.GetRepository().Threshold = Level.All;

            Log.Debug("Initialized log4net");
         }
      }
   }
}